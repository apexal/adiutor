# README #

A soon-to-be social media-like academic-tracker free web service for Regis High School!

### What is Adiutor? ###

* adiutor, adiutoris (m) - helper, assistant
* v0.1.0
* Make Regis eas*ier*
* Track homework, tests, quizzes, grades, notes, tasks, reminders, schedules, meetings, clubs, groups, and more all in one place at the same time.

### How do I get set up? ###
* Clone this repository
* Intall Mongo DB
* npm install
* npm run scrape (This takes a looooong time)
* npm run populate
* npm start
* Go to http://localhost:3000